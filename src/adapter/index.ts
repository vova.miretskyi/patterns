interface IPhone {
  useLightning(): void;
}

interface Android {
  useMicroUSB(): void;
}

class IPhone7 implements IPhone {
  useLightning() {
    console.log("using lightning port");
  }
}

class GooglePixel implements Android {
  useMicroUSB() {
    console.log("Using micro USB...");
  }
}

class LightningToMicroUSBAdapter implements Android {
  private iphoneDevice: IPhone;

  constructor(iphone: IPhone) {
    this.iphoneDevice = iphone;
  }

  useMicroUSB() {
    console.log("want to use micro USB, converting to Lightning...");

    this.iphoneDevice.useLightning();
  }
}

let iphone = new IPhone7();
let chargeAdapter = new LightningToMicroUSBAdapter(iphone);

chargeAdapter.useMicroUSB();
