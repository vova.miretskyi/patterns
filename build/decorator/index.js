"use strict";
class HeadPhones {
    getTitle() {
        return this.title;
    }
}
class AirPods extends HeadPhones {
    constructor() {
        super(...arguments);
        this.title = "AirPods";
    }
    cost() {
        return 500;
    }
}
class HeadPhonesOptions extends HeadPhones {
}
class WirelessHeadPhones extends HeadPhonesOptions {
    constructor(headphones) {
        super();
        this.decoratedHeadPhones = headphones;
    }
    getTitle() {
        return this.decoratedHeadPhones.getTitle() + ", wireless";
    }
    cost() {
        return this.decoratedHeadPhones.cost() + 100;
    }
}
let myHeadPhones = new AirPods();
console.log("myHeadPhones.title 1", myHeadPhones.getTitle());
console.log("myHeadPhones.cost 1", myHeadPhones.cost());
myHeadPhones = new WirelessHeadPhones(myHeadPhones);
console.log("myHeadPhones.title 2", myHeadPhones.getTitle());
console.log("myHeadPhones.cost 2", myHeadPhones.cost());
