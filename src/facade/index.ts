//clean the room

class VacuumCleaner {
  turnOn() {
    console.log("Vacuum Cleaner: Turned on");
  }

  turnOff() {
    console.log("Vacuum Cleaner: Turned off");
  }

  clearDust() {
    console.log("Clear dust");
  }
}

class WaterCleaning {
  fillBucketWithWater() {
    console.log("Bucket is full of water");
  }

  addSoap() {
    console.log("Added Soap to the water in the bucket");
  }

  cleanTheFloor() {
    console.log("Clean the floor");
  }

  pourOutWater() {
    console.log("Bucket is empty");
  }
}

class ClearRoomFacade {
  constructor(
    private vacuumCleaner: VacuumCleaner,
    private waterCleaning: WaterCleaning
  ) {}

  clearRoom() {
    this.vacuumCleaner.turnOn();
    this.vacuumCleaner.clearDust();
    this.vacuumCleaner.turnOff();

    this.waterCleaning.fillBucketWithWater();
    this.waterCleaning.addSoap();
    this.waterCleaning.cleanTheFloor();
    this.waterCleaning.pourOutWater();
  }
}

const vacuumCleaner = new VacuumCleaner();
const waterCleaning = new WaterCleaning();

const clearRoom = new ClearRoomFacade(vacuumCleaner, waterCleaning);

clearRoom.clearRoom();
