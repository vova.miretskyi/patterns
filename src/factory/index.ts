interface IProps {
  name: string;
  type: "teacher" | "assistant" | "custodian";
}

class Teacher {
  constructor(public props: IProps) {}
}

class Assistant {
  constructor(public props: IProps) {}
}

class Custodian {
  constructor(public props: IProps) {}
}

class StaffFactory {
  create(data: IProps) {
    let result;
    switch (data.type) {
      case "teacher": {
        result = new Teacher(data);
        break;
      }
      case "assistant": {
        result = new Assistant(data);
        break;
      }
      case "custodian": {
        result = new Custodian(data);
        break;
      }
    }

    return result;
  }
}

const school = new StaffFactory();

console.log(
  "Miss Jenny",
  school.create({ type: "teacher", name: "Miss Jenny" }).props
);
console.log(
  "Mister Wow",
  school.create({ type: "assistant", name: "Mister Wow" }).props
);
console.log(
  "Miss Oh",
  school.create({ type: "custodian", name: "Miss Oh" }).props
);
