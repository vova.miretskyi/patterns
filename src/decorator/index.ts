abstract class HeadPhones {
  public title!: string;

  public getTitle(): string {
    return this.title;
  }

  public abstract cost(): number;
}

class AirPods extends HeadPhones {
  public title: string = "AirPods";

  public cost(): number {
    return 500;
  }
}

abstract class HeadPhonesOptions extends HeadPhones {
  decoratedHeadPhones!: HeadPhones;
  public abstract getTitle(): string;
  public abstract cost(): number;
}

class WirelessHeadPhones extends HeadPhonesOptions {
  constructor(headphones: HeadPhones) {
    super();
    this.decoratedHeadPhones = headphones;
  }

  public getTitle(): string {
    return this.decoratedHeadPhones.getTitle() + ", wireless";
  }

  public cost(): number {
    return this.decoratedHeadPhones.cost() + 100;
  }
}

let myHeadPhones = new AirPods();

console.log("myHeadPhones.title 1", myHeadPhones.getTitle());
console.log("myHeadPhones.cost 1", myHeadPhones.cost());

myHeadPhones = new WirelessHeadPhones(myHeadPhones);

console.log("myHeadPhones.title 2", myHeadPhones.getTitle());
console.log("myHeadPhones.cost 2", myHeadPhones.cost());
