interface Bus {
  get getTotalSeats(): number;
  get getRoute(): string;
  go(): void;
}

interface Subject {
  registerObserver(o: Observer): void;
  removeObserver(o: Observer): void;
  notifyObservers(): void;
}

interface Observer {
  update(time: number): void;
}

class BusStation implements Subject {
  private time!: number;
  private observers: Observer[] = [];

  setTime(time: number) {
    console.log("current time is: ", time);
    this.time = time;
    this.notifyObservers();
  }

  registerObserver(o: Observer): void {
    this.observers.push(o);
  }

  removeObserver(o: Observer): void {
    const index = this.observers.indexOf(o);
    this.observers.splice(index, 1);
  }

  notifyObservers(): void {
    for (let observer of this.observers) {
      observer.update(this.time);
    }
  }
}

class BusA implements Bus, Observer {
  private subject!: Subject;
  private readonly TOTAL_SEATS: number = 26;
  private route: string = "Ternopil - Yaremche";

  constructor(private busStation: Subject) {
    this.subject = busStation;
    this.subject.registerObserver(this);
  }

  update(time: number): void {
    if (time === 3 || time === 5) {
      this.go();
    }
  }

  get getTotalSeats(): number {
    return this.TOTAL_SEATS;
  }
  get getRoute(): string {
    return this.route;
  }

  go(): void {
    console.log("-----------------------");
    console.log("BusA started going");
    console.log(`Route: ${this.getRoute}`);
    console.log(`Total seats: ${this.getTotalSeats}`);
    console.log("-----------------------");
  }
}

class BusB implements Bus, Observer {
  private subject!: Subject;
  private readonly TOTAL_SEATS: number = 18;
  private route: string = "Pidkamin - Lviv";

  constructor(private busStation: Subject) {
    this.subject = busStation;
    this.subject.registerObserver(this);
  }

  update(time: number): void {
    if (time === 5) {
      this.go();
    }
  }

  get getTotalSeats(): number {
    return this.TOTAL_SEATS;
  }
  get getRoute(): string {
    return this.route;
  }

  go(): void {
    console.log("-----------------------");
    console.log("BusA started going");
    console.log(`Route: ${this.getRoute}`);
    console.log(`Total seats: ${this.getTotalSeats}`);
    console.log("-----------------------");
  }
}

const busStation = new BusStation();

const busA = new BusA(busStation);
const busB = new BusB(busStation);

busStation.setTime(3);
busStation.setTime(5);
