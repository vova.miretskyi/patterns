interface State {
  order: Order;
  cancelOrder(): void;
  verifyOrder(): void;
  shipOrder(): void;
}

class Order {
  cancelledOrderState!: State;
  paymentPendingState!: State;
  orderBeingPreparedState!: State;
  orderShippedState!: State;

  private currentState!: State;

  constructor() {
    this.cancelledOrderState = new CancelOrderState(this);
    this.paymentPendingState = new PaymentPendingState(this);
    this.orderBeingPreparedState = new OrderBeingPreparedState(this);
    this.orderShippedState = new OrderShippedState(this);

    this.setState(this.paymentPendingState);
  }

  setState(state: State) {
    this.currentState = state;
  }

  get getState(): State {
    return this.currentState;
  }
}

class PaymentPendingState implements State {
  order!: Order;
  constructor(order: Order) {
    this.order = order;
  }
  cancelOrder(): void {
    console.log("Canceling your unpaid order");
    this.order.setState(this.order.cancelledOrderState);
  }
  verifyOrder(): void {
    console.log("Payment verified! Shipping soon...");
    this.order.setState(this.order.orderBeingPreparedState);
  }
  shipOrder(): void {
    console.log("Cannot ship the order when payment is pending!");
  }
}

class CancelOrderState implements State {
  order!: Order;
  constructor(order: Order) {
    this.order = order;
  }
  cancelOrder(): void {
    console.log("Your order has already been cancelled");
  }
  verifyOrder(): void {
    console.log("Order cancelled, you cannot verify payment");
  }
  shipOrder(): void {
    console.log("Order cannot ship, it was cancelled");
  }
}

class OrderBeingPreparedState implements State {
  order!: Order;
  constructor(order: Order) {
    this.order = order;
  }
  cancelOrder(): void {
    console.log("Cancelling order");
    this.order.setState(this.order.cancelledOrderState);
  }
  verifyOrder(): void {
    console.log("Payment already verified");
  }
  shipOrder(): void {
    console.log("Shipping your order now");
    this.order.setState(this.order.orderShippedState);
  }
}

class OrderShippedState implements State {
  order!: Order;
  constructor(order: Order) {
    this.order = order;
  }
  cancelOrder(): void {
    console.log("You cannot cancel shipped order");
  }
  verifyOrder(): void {
    console.log("You cannot verify payment of shipped order");
  }
  shipOrder(): void {
    console.log("You cannot ship shipped order");
  }
}

let order = new Order();
order.getState.verifyOrder();
order.getState.shipOrder();
order.getState.cancelOrder();
console.log("Order state:", (<any>order.getState).constructor.name);
